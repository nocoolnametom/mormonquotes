<?php

namespace MormonQuotes;

class Display
{
    const SEARCH_KEY = 'one';
    private $link;
    private $search;

    public function __construct(array $getVar, string $dbName, string $mysqlUser, string $mysqlPassword = '', string $dbHost = '')
    {
        $dbHost = '' === $dbHost ? 'localhost' : $dbHost;
        if ('.' === $dbHost) {
            $this->link = new \mysqli($dbHost, $mysqlUser, $mysqlPassword, $dbName, (int) ini_get('mysqli.default_port'), 'mysql');
        } else {
            $this->link = new \mysqli($dbHost, $mysqlUser, $mysqlPassword, $dbName);
        }

        if ($this->link->connect_error) {
            die('Connect Error ('.$this->link->connect_errno.') '
              .$this->link->connect_error);
        }

        if (array_key_exists(self::SEARCH_KEY, $getVar) && strpos($getVar[self::SEARCH_KEY], ' ')) {
            header('Location:'.str_replace(' ', '_', $getVar[self::SEARCH_KEY]).'');
        }

        $this->search = str_replace('_', ' ', mysqli_real_escape_string($this->link, $getVar[self::SEARCH_KEY] ?? ''));
    }

    public function build_page(): string
    {
        if (empty($this->search)) {
            $title = 'Mormon Quotes';

            return $this->build_simple_page($title);
        }

        $author_scout_sql =
          "SELECT * 
          FROM authors 
          WHERE author_name LIKE '{$this->search}'
          LIMIT 1";
        $author_scout_query = mysqli_query($this->link, $author_scout_sql);

        $tag_scout_sql =
          "SELECT * 
          FROM tags 
          WHERE tag_name LIKE '{$this->search}'
          LIMIT 1";
        $tag_scout_query = mysqli_query($this->link, $tag_scout_sql);

        $query = null;
        $title = null;

        if ($author_scout_query && mysqli_num_rows($author_scout_query)) {
            $author_id = null;
            while ($row = mysqli_fetch_array($author_scout_query)) {
                if ($row['author_name'] !== $this->search) {
                    $header = 'Location:https://mormonquotes.com/'.str_replace(' ', '_', (string) $row['author_name']);
                    header($header);
                }
                $title = $row['author_name'];
                $author_id = (int) $row['author_id'];
                $get_quotes_for_author_sql =
                  "SELECT *
                  FROM quotes
                  INNER JOIN authors ON quotes.quote_author_id = authors.author_id
                  WHERE quote_author_id = '${author_id}'
                  ORDER BY quote_author_id";
                $get_quotes_for_author_query = mysqli_query($this->link, $get_quotes_for_author_sql);
                $query = $get_quotes_for_author_query;
            }

            return $this->build_regular_page($query, $title, $author_id);
        }
        if ($tag_scout_query && mysqli_num_rows($tag_scout_query)) {
            while ($row = mysqli_fetch_array($tag_scout_query)) {
                if (ucfirst($row['tag_name']) !== $this->search) {
                    $header = 'Location:https://mormonquotes.com/'.ucfirst(str_replace(' ', '_', (string) $row['tag_name']));
                    header($header);
                }
                $title = ucfirst($row['tag_name']);
                $tag_id = $row['tag_id'];
                $get_quotes_for_tag_sql =
              "SELECT *
              FROM stitches
              INNER JOIN quotes ON stitches.stitch_quote_id = quotes.quote_id
              INNER JOIN authors ON quotes.quote_author_id = authors.author_id
              WHERE stitch_tag_id = ${tag_id}
              ORDER BY quote_author_id";
                $get_quotes_for_tag_query = mysqli_query($this->link, $get_quotes_for_tag_sql);
                $query = $get_quotes_for_tag_query;
            }

            return $this->build_regular_page($query, $title);
        }

        return '404<br><a href="/">Return to Mormon Quotes</a>';
    }

    public function build_simple_page(string $title): string
    {
        return <<<EOF
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html lang='en'>
          <head>
            <title>{$title}</title>
            <meta http-equiv='Content-type' content='text/html; charset=iso-8859-1'>
            <meta name='description' content='A resource for studying the doctrine, history, and culture of the Mormon church through the words of its leaders.'>
            <meta name='keywords' content='Mormon Quotes,Mormon,quotes,LDS,Latter-Day Saints,prophets,Joseph Smith,Brigham Young,prophets,history,Christianity'>
            <link rel='stylesheet' type='text/css' href='css/core.css'>
            <link href='//fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
          </head>
          <body>
            <div class='w'>
              <div class='group' style='font-size:36px;font-weight:bold;'>
                <a href='/' title='Mormon Quotes home'>Mormon Quotes</a>
              </div>
              <hr>
              {$this->build_plain_header()}
              <div style='padding:10px 0 0 0;text-align:right;font-size:10px;color:#aaa;'>
              &copy; 2011
              </div>
            </div>
          </body>
        </html>
EOF;
    }

    public function build_regular_page($query, string $title, int $author_id = null): string
    {
        return <<<EOF
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html lang='en'>
          <head>
            <title>{(${author_id} ? "Quotes from ":"") . ${title})} | Mormon Quotes</title>
            <meta http-equiv='Content-type' content='text/html; charset=iso-8859-1'>
            <meta name='description' content='A resource for studying the doctrine, history, and culture of the Mormon church through the words of its leaders.'>
            <meta name='keywords' content='Mormon Quotes,Mormon,quotes,LDS,Latter-Day Saints,prophets,Joseph Smith,Brigham Young,prophets,history,Christianity'>
            <link rel='stylesheet' type='text/css' href='css/core.css'>
            <link href='//fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
          </head>
          <body>
            <div class='w'>
              <div class='group' style='font-size:36px;font-weight:bold;'>
                <a href='/' title='Mormon Quotes home'>Mormon Quotes</a>
              </div>
              <hr>
              {$this->build_quote_header($query, $title)}
              <div style='padding:10px 0 0 0;text-align:right;font-size:10px;color:#aaa;'>
              &copy; 2011
              </div>
            </div>
          </body>
        </html>
EOF;
    }

    public function build_plain_header(): string
    {
        return "
        <h1 style='display:none;'>Mormon Quotes</h1>
        <h2>Leaders</h2>
        {$this->print_people()}
        <hr>
        <h2>Topics</h2>
        {$this->print_topics()}";
    }

    public function build_quote_header($query, string $title): string
    {
        return "
        <h1 style='text-align:center;'>{$title}</h1>
        <div class='content group'>
          {$this->print_quotes($query)}
        </div>";
    }

    public function print_people(): string
    {
        $output = "<div class='group'>";
        $get_authors_sql =
          "SELECT * 
          FROM authors 
          WHERE author_quotes_count > 2 
          AND author_notes LIKE '%leader%'
          AND author_image <> ''
          ORDER BY author_quotes_count DESC";
        $get_authors_query = mysqli_query($this->link, $get_authors_sql);
        if ($get_authors_query) {
            while ($row = mysqli_fetch_array($get_authors_query)) {
                $author_name = $row['author_name'];
                $author_url = str_replace(' ', '_', $author_name);
                $author_image = $row['author_image'];
                $author_quotes_count = $row['author_quotes_count'];
                $output .= "
              <div class='portrait'>
                <a href='/{$author_url}' title='Quotes from {$author_name}'>
                  <img alt='{$author_name}' src='//i.imgur.com/{$author_image}s.jpg' width='90' height='90'/>
                  <div class='portraitLabel'>{$author_name} ({$author_quotes_count})</div>
                </a>
              </div>";
            }
        }

        return $output.'</div>';
    }

    public function print_topics(): string
    {
        $output = "<div class='group'><ul class='menuList'>";
        $get_tags_sql =
          'SELECT *
          FROM tags
          WHERE tag_tagged_count > 4
          ORDER BY tag_tagged_count DESC';
        $get_tags_query = mysqli_query($this->link, $get_tags_sql);
        if ($get_tags_query) {
            while ($row = mysqli_fetch_array($get_tags_query)) {
                $tag_name = ucfirst($row['tag_name']);
                $tag_url = str_replace(' ', '_', $tag_name);
                $tag_count = $row['tag_tagged_count'];
                $output .= "
            <li>
              <a href='/{$tag_url}' title='{$tag_name} quotes'>{$tag_name}</a> <span>{$tag_count}</span>
            </li>
          ";
            }
        }

        return '</ul></div>';
    }

    public function print_quotes($gQ): string
    {
        $output = "<div class='quotes'>";
        $i = 0;
        while ($row = mysqli_fetch_array($gQ)) {
            ++$i;
            $author_id = $row['author_id'];
            $author_name = $row['author_name'];
            $author_url = str_replace(' ', '_', $author_name);
            $author_image = $row['author_image'];
            if (null === $author_image || '' === $author_image) {
                $author_image = 'J3GgZ';
            }
            $author_discourses_count = $row['author_discourses_count'];
            $quote_text = $row['quote_text'];
            $quote_source = $row['quote_source'];
            $output .= "
              <div class='block group'>
              <div class='left'>
                <img alt='{$author_name}' src='//i.imgur.com/{$author_image}s.jpg' width='60' height='60'/>
              </div>
              <div class='right'>
                <div class='quote'>
                  <div class='text'>{$quote_text}</div>
                  <div class='profile'>
                    <a href='/{$author_url}'>{$author_name}</a>,
                    <i>{$quote_source}</i>";
            if (array_key_exists('source', $row) && false !== strpos($row['source'], 'Journal of Discourses')) {
                $output .= " <a class='jod' href='//journalofdiscourses.com'>(JournalOfDiscourses.com)</a>";
            }
            $output .= '
                    </div>
                  </div>
                </div>
              </div>';
        }

        return $output.'</div>';
    }
}
